TallyJ 1.85
------
Original version of TallyJ. For a the current version, please see https://tallyJ.apphb.com/

Old
------
For support and updates, visit the TallyJ Support Page on Facebook: http://www.facebook.com/pages/TallyJ-Support-Page/
              

Requirements for TallyJ
-----------------------
- Windows XP, Vista or 7 (or later)
- Internet Explorer 8
- Microsoft XML Core Services 6

Installing TallyJ
-----------------
- Copy all files into a new folder on your computer

Running TallyJ
--------------
- Open the "TallyJ.HTA" file
- Further instructions can be found in the Instructions_EN.htm file.